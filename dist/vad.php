<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Contact</title>
    <meta name="description" content="text">
    <?php include 'parts/main/head.php'; ?>

</head>

<body>
    <div class="page__wrap">
        <?php include 'parts/main/header.php'; ?>

        <div class="page__container page__container--default page__container--ved">
            <div class="container">
                <h1 class="title_y">АУТСОРСИНГ ВЭД</h1>
                <div class="ved__block_1">
                    <div class="ved__block_1__info">
                        <div class="default_text">
                            <p>Аутсорсинг внешне экономической деятельности (ВЭД) - это передача определённых функций и бизнес-процессов другому предприятию, решающему данные вопросы на профессиональном уровне.Кому необходим аутсорсинг ВЭД? Аутсорсинг ВЭД используют предприятия, которые хотят минимизировать свои риски, получив при этом максимальную выгоду за счет снижения цены.</p>

                            <p><b>Данные услуги актуальны:</b></p>

                            <ul>
                                <li>для компаний, которые впервые выходят на внешнеэкономический рынок и не знакомы с особенностями ведения ВЭД с Россией;</li>
                                <li>для компаний, у которых объем периодичность поставок велики, но участник ВЭД хочет сконцентрироваться на профильной деятельности;</li>
                                <li>для компаний, которые не хотят создавать собственный отдел ВЭД, тем самым желая снизить операционные расходы и риски;</li>
                                <li>для компаний, которые хотят сократить финансовые и временные затраты на осуществление ВЭД.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="ved__block_1__form">
                        <div class="ved__block_1__form__title">Получите консультацию</div>
                        <div class="ved__block_1__form__contact">
                            <div class="ved__block_1__form__contact__text">Свяжитесь с нами по телефону</div>
                            <div class="ved__block_1__form__contact__phone">
                                <a href="tel8 (3812) 38-61-63">8 (3812) 38-61-63</a>
                            </div>
                        </div>
                        <div class="ved__block_1__form__content">
                            <div class="ved__block_1__form__content__title">Либо оставьте свой телефон</div>
                            <form action="">
                                <input type="phone" name="phone" required placeholder="Телефон" class='ved__block_1__form__field'>
                                <button class="btn_default btn_glow ved__block_1__form__btn"><span>Отправить</span></button>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <?php include 'parts/main/footer.php'; ?>
    </div>
</body>

</html>