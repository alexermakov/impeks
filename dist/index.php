<?php $homePage = true;?>

<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>page_name</title>
    <meta name="description" content="text">
    <?php include 'parts/main/head.php'; ?>

</head>

<body class='home body-lock'>
    <div class="page__wrap">
        <?php include 'parts/main/header.php'; ?>

        <div class="page__container page__container--home">
            <?php include 'parts/pages/home/hero.php'; ?>
            <?php include 'parts/pages/home/about.php'; ?>
            <?php include 'parts/pages/home/products.php'; ?>
            <?php include 'parts/pages/home/advantage.php'; ?>
            <?php include 'parts/pages/home/portfolio.php'; ?>
            <?php include 'parts/pages/home/sertificates.php'; ?>
            <?php $companies_class = 'companies_block--home';?>
            <?php include 'parts/pages/home/partners.php'; ?>
        </div>

        <?php include 'parts/main/footer.php'; ?>
    </div>
</body>

</html>