<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Product</title>
    <meta name="description" content="text">
    <?php include 'parts/main/head.php'; ?>

</head>

<body>
    <div class="page__wrap">
        <?php include 'parts/main/header.php'; ?>

        <div class="page__container page__container--default page__container--product">
            <div class="breadcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="">Главная</a></li>
                        <li><a href="">Каталог</a></li>
                        <li><a href="">Видеонаблюдение</a></li>
                        <li><a href="">WISENET</a></li>
                        <li><a href="">Аксессуары</a></li>
                        <li><a href="">Термокожухи, накладки</a></li>
                        <li>Термокожух Wisenet SHB-9000H</li>
                    </ul>
                </div>
            </div>

            <div class="page__content__product">
                <div class="container">
                    <div class="pproduct__top">

                        <div class="title_y title_y--product title_y--product--mobile">Термокожух Wisenet SHB-9000H</div>
                        <div class="pproduct__image__block">

                            <div class="pproduct__image__block__main js_pproduct__image__block__main">
                                <div class="pproduct__image__block__main__inner">
                                    <img src="images/__content/pages/product/main.jpg" alt="">
                                </div>
                                <div class="pproduct__image__block__main__more" data-fancybox data-src="images/__content/pages/product/main.jpg">
                                    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6.53425 13.4709C7.94477 13.4709 9.26182 13.012 10.3324 12.2473L14.3601 16.2749C14.547 16.4618 14.7934 16.5553 15.0483 16.5553C15.6006 16.5553 16 16.1304 16 15.5866C16 15.3317 15.915 15.0938 15.7281 14.9069L11.726 10.8962C12.5672 9.79162 13.0685 8.42359 13.0685 6.9366C13.0685 3.34233 10.1285 0.402344 6.53425 0.402344C2.93149 0.402344 0 3.34233 0 6.9366C0 10.5309 2.93149 13.4709 6.53425 13.4709ZM6.53425 12.0603C3.72172 12.0603 1.41052 9.74063 1.41052 6.9366C1.41052 4.13256 3.72172 1.81286 6.53425 1.81286C9.33829 1.81286 11.658 4.13256 11.658 6.9366C11.658 9.74063 9.33829 12.0603 6.53425 12.0603Z" fill="#161616" fill-opacity="0.5" />
                                    </svg>
                                    <span>Увеличить</span>
                                </div>
                            </div>

                            <div class="pproduct__image__block__add js_pproduct__image__block__add">
                                <div class="pproduct__image__block__add__item">
                                    <div class="pproduct__image__block__add__item__inner">
                                        <img src="images/__content/pages/product/main.jpg" alt="">
                                    </div>
                                </div>
                                <div class="pproduct__image__block__add__item">
                                    <div class="pproduct__image__block__add__item__inner">
                                        <img src="images/__content/pages/product/main.jpg" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="pproduct__info__block">
                            <h1 class="title_y title_y--product title_y--product--desktop">Термокожух Wisenet SHB-9000H</h1>
                            <div class="pproduct__info__block__char">
                                <div class="pproduct__info__block__char__title">Основные характеристики:</div>
                                <ul>
                                    <li>Совместимость с камерой TNB-9000</li>
                                    <li>Материал- Корпус/солнцезащитный козырек: алюминий- Передняя и задняя крышки: АБС-пластик</li>
                                    <li>Рабочая температура: -50°C - +60°C (-58°F - +140°F) / относительная влажность 10-100% (с образованием конденсата)</li>
                                    <li>Масса: 6,2 кг (13,67 фунта)</li>
                                    <li>Цвет: белый</li>
                                    <li>IP66</li>
                                </ul>
                            </div>
                            <div class="pproduct__info__block__price">
                                <div class="pproduct__info__block__price__title">Цена:</div>
                                <div class="pproduct__info__block__price__value">2 941 $</div>
                            </div>
                            <div class="pproduct__info__block__docs">
                                <div class="pproduct__info__block__docs__item">
                                    <div class="pproduct__info__block__docs__item__title">Документация</div>
                                    <a href='' class="pproduct__info__block__docs__item__x" download>
                                        <div class="pproduct__info__block__docs__item__x__image">
                                            <img src="images/icons/pdf.svg" alt="">
                                        </div>
                                        <div class="pproduct__info__block__docs__item__x__title">Скачать, DataSheet_SHB-9000H_RU.pdf</div>
                                    </a>
                                </div>
                                <div class="pproduct__info__block__docs__item">
                                    <div class="pproduct__info__block__docs__item__title">Сертификаты</div>
                                    <a href='' class="pproduct__info__block__docs__item__x" download>
                                        <div class="pproduct__info__block__docs__item__x__image">
                                            <img src="images/icons/pdf.svg" alt="">
                                        </div>
                                        <div class="pproduct__info__block__docs__item__x__title">Скачать, DataSheet_SHB-9000H_RU.pdf</div>
                                    </a>
                                </div>
                                <div class="pproduct__info__block__docs__item">
                                    <div class="pproduct__info__block__docs__item__title">Руководство по эксплуатации</div>
                                    <a href='' class="pproduct__info__block__docs__item__x" download>
                                        <div class="pproduct__info__block__docs__item__x__image">
                                            <img src="images/icons/pdf.svg" alt="">
                                        </div>
                                        <div class="pproduct__info__block__docs__item__x__title">Скачать, DataSheet_SHB-9000H_RU.pdf</div>
                                    </a>
                                </div>
                            </div>

                            <div class="pproduct__info__block__order">
                                <a href="" class="btn_default btn_glow btn_product_order"><span>Оставить заявку</span></a>
                                <div class="pproduct__info__block__order__add_text">Чтобы узнать актуальную цену товара, оставьте заявку</div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>

        <?php $companies_class = 'companies_block--product';?>
        <?php include 'parts/pages/home/partners.php'; ?>

    </div>

    <?php include 'parts/main/footer.php'; ?>
    </div>
</body>

</html>