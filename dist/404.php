<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>404</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body>
    <div class="page__wrap">
        <?php include 'parts/main/header.php'; ?>

        <div class="page__container page__container--400">
            <div class="breadcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="">Главная</a></li>
                        <li>Страница 404</li>
                    </ul>
                </div>
            </div>

            <div class="page__content">
                <div class="container">
                    <div class="content_404">
                        <h1 class="title_x title_x--404">404</h1>
                        <div class="content_404__text">Страница не найдена</div>
                        <a href="" class="btn_default btn_x btn_404"><span>Вернуться на главную</span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'parts/main/footer.php'; ?>
    </div>
</body>

</html>