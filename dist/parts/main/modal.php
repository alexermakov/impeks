<div class="js__modal js__modal--menu modal__x modal__menu">
    <div class="container">
        <div class="modal__menu__inner">

            <div class="modal__menu__top">

                <div class="header__search">
                    <button class="js_header__search__btn header__search__btn">
                        <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M26.6658 26.6668L21.2658 21.2668M21.2658 21.2668C22.1326 20.4001 22.8201 19.3711 23.2892 18.2387C23.7582 17.1063 23.9996 15.8926 23.9996 14.6668C23.9996 13.4411 23.7582 12.2274 23.2892 11.0949C22.8201 9.96249 22.1326 8.93354 21.2658 8.06682C20.3991 7.20009 19.3702 6.51257 18.2377 6.0435C17.1053 5.57443 15.8916 5.33301 14.6658 5.33301C13.4401 5.33301 12.2264 5.57443 11.0939 6.0435C9.96152 6.51257 8.93257 7.20009 8.06584 8.06682C6.31541 9.81725 5.33203 12.1913 5.33203 14.6668C5.33203 17.1423 6.31541 19.5164 8.06584 21.2668C9.81627 23.0172 12.1904 24.0006 14.6658 24.0006C17.1413 24.0006 19.5154 23.0172 21.2658 21.2668Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                </div>


                <a href="" class="modal__menu__logo">
                    <img src="images/logo.svg" alt="main logo">
                </a>
                <button class="modal__menu__btn_close btn__close js_modal__btn_close" aria-label="Close menu"></button>
            </div>

            <div class="modal__menu__content">
                <div class="modal__menu__list">
                    <div class="modal__menu__section modal__menu__section--1">
                        <div class="modal__menu__section__title">Каталог</div>
                        <ul>
                            <li><a href="">Видеонаблюдение</a></li>
                            <li><a href="">Видеорегистраторы</a></li>
                            <li><a href="">СКУД</a></li>
                            <li><a href="">Охранное освещение</a></li>
                            <li><a href="">Охрана периметра</a></li>
                            <li><a href="">ПО</a></li>
                            <li><a href="">Сетевое обуродование</a></li>
                            <li><a href="">IP-телефония</a></li>
                            <li><a href="">Кабель</a></li>
                            <li><a href="">Блоки питания</a></li>
                            <li><a href="">Шлагбаумы</a></li>
                        </ul>
                    </div>
                    <div class="modal__menu__section modal__menu__section--2">
                        <div class="modal__menu__section__title">
                            <a href="">Продукты</a>
                        </div>
                        <ul>
                            <li><a href="">ВСО “Точка-С”</a></li>
                            <li><a href="">АСО “КОНТУР-С”</a></li>
                        </ul>
                    </div>
                    <div class="modal__menu__section modal__menu__section--3">
                        <div class="modal__menu__section__title">
                            <a href="">Решения</a>
                        </div>
                        <ul>
                            <li><a href="">Банки</a></li>
                            <li><a href="">Нефтегазовый сектор</a></li>
                            <li><a href="">Образовательные объекты</a></li>
                            <li><a href="">Объект культуры</a></li>
                            <li><a href="">Объекты торговли</a></li>
                            <li><a href="">Объекты транспорта</a></li>
                            <li><a href="">Офисные центры</a></li>
                            <li><a href="">Промышленые объекты</a></li>
                            <li><a href="">Спортивные объекты</a></li>
                        </ul>
                    </div>
                    <div class="modal__menu__section modal__menu__section--4">
                        <div class="modal__menu__section__title">
                            <a href="">Аутсорсинг ВЭД</a>
                        </div>
                        <ul class='modal__menu__section__menu--blue'>
                            <li><a href="">Проекты</a></li>
                            <li><a href="">Новости</a></li>
                            <li><a href="">Бренды</a></li>
                            <li><a href="">Документы</a></li>
                            <li><a href="">Контакты</a></li>
                        </ul>

                        <a href="#" class="modal__menu__btn__link btn_default btn_glow">
                            <span>Сертификаты</span>
                        </a>
                    </div>
                </div>

                <div class="modal__menu__contact">
                    <div class="modal__menu__contact__info">
                        <p>Компания ООО «Импэкс-Групп» - это стабильная, высокотехнологичная, динамично развивающаяся компания, которая осуществляет комплексные поставки оборудования для построения систем безопасности.</p>
                    </div>
                    <div class="modal__menu__contact__item modal__menu__contact__item--link"><a href="tel:+73812386163">+7 (3812) 38-61-63</a></div>
                    <div class="modal__menu__contact__item modal__menu__contact__item--link"><a href="mailto:info@impeks-group.ru">info@impeks-group.ru</a></div>
                    <div class="modal__menu__contact__item"><p>Россия, г. Омск, ул. К. Маркса 41/1, офис 451</p></div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="js__modal js__modal--search modal__x modal__search">
    <div class="container">
        <div class="modal__search__inner">
            <div class="modal__menu__top modal__menu__top--search">
                <a href="" class="modal__menu__logo">
                    <img src="images/logo.svg" alt="main logo">
                </a>
                <button class="modal__menu__btn_close modal__menu__btn_close--search btn__close js_modal__btn_close" aria-label="Close search">
                    <svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15 6.75C15.4142 6.75 15.75 6.41421 15.75 6C15.75 5.58579 15.4142 5.25 15 5.25V6.75ZM0.46967 5.46967C0.176777 5.76256 0.176777 6.23744 0.46967 6.53033L5.24264 11.3033C5.53553 11.5962 6.01041 11.5962 6.3033 11.3033C6.59619 11.0104 6.59619 10.5355 6.3033 10.2426L2.06066 6L6.3033 1.75736C6.59619 1.46447 6.59619 0.989593 6.3033 0.696699C6.01041 0.403806 5.53553 0.403806 5.24264 0.696699L0.46967 5.46967ZM15 5.25L1 5.25V6.75L15 6.75V5.25Z" fill="white"/>
                    </svg>
                </button>
            </div>


            <div class="modal__search__main">
                <div class="modal__search__form">
                    <form action="javascript:void(0)" class='js_form_search'>
                        <button class='modal__search__form__btn__search' type='submit'  aria-label="Send search form">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.22204 17.699C11.073 17.699 12.8013 17.0969 14.2062 16.0933L19.4915 21.3786C19.7368 21.6239 20.0601 21.7465 20.3946 21.7465C21.1194 21.7465 21.6435 21.189 21.6435 20.4754C21.6435 20.1409 21.532 19.8287 21.2867 19.5834L16.0349 14.3204C17.1388 12.8709 17.7966 11.0757 17.7966 9.12439C17.7966 4.40781 13.9386 0.549805 9.22204 0.549805C4.49432 0.549805 0.647461 4.40781 0.647461 9.12439C0.647461 13.841 4.49432 17.699 9.22204 17.699ZM9.22204 15.848C5.53129 15.848 2.49841 12.804 2.49841 9.12439C2.49841 5.44479 5.53129 2.40075 9.22204 2.40075C12.9016 2.40075 15.9457 5.44479 15.9457 9.12439C15.9457 12.804 12.9016 15.848 9.22204 15.848Z" fill="#DBDBDB"/>
                            </svg>
                        </button>
                        <input type="search" name="s" required placeholder='' class='modal__search__form__field js_form_search__field' autocomplete="on">
                        <button class='modal__search__form__btn__search modal__search__form__btn__search--reset js_btn__search--reset' type='reset'  aria-label="Clear search form">
                            <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M2 1.58008L15.4328 15.0129M2 15.0129L15.4328 1.58008" stroke="#DBDBDB" stroke-width="2.68657" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </form>
                </div>


                <div class="modal__search__result__list js_modal__search__result__list--none">
                    <div class="modal__search__result__list__inner">
                        <div class="modal__search__result__list__none">
                            <div class="modal__search__result__list__title">Ничего не найдено</div>
                            <div class="modal__search__result__list__text">Попробуйте снова</div>
                        </div>
                    </div>
                </div>

                <div class="modal__search__result__list js_modal__search__result__list">
                    <div class="modal__search__result__list__inner">
                        <div class="modal__search__result__exist">
                            <div class="modal__search__result__exist__list">
                                <div class="modal__search__result__block">
                                    <div class="modal__search__result__block__title">Продукты</div>
                                    <div class="modal__search__result__block__list">
                                        <ul>
                                            <li>
                                                <a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="modal__search__result__block">
                                    <div class="modal__search__result__block__title">Каталог</div>
                                    <div class="modal__search__result__block__list">
                                        <div class="modal__search__result__block__list">
                                            <ul>
                                                <li>
                                                    <a href="">
                                                        <img src="images/__content/any/search_result.jpg" alt="Вибрационное средство обнаружения «Точка-С»">
                                                        <span>Вибрационное средство обнаружения «Точка-С»</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal__search__result__block">
                                    <div class="modal__search__result__block__title">Статьи</div>
                                    <div class="modal__search__result__block__list">
                                        <ul>
                                            <li><a href=""><span>Все о вибрационном средстве обнаружения «Точка-С»</span></a></li>
                                            <li><a href=""><span>Все о вибрационном средстве обнаружения «Точка-С»</span></a></li>
                                            <li><a href=""><span>Все о вибрационном средстве обнаружения «Точка-С»</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal__search__result__list__bottom">
                                <a href="" class="btn_default btn_glow"><span>Показать все</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<!-- окно Call-->

<div id="js_modal__call" class="modal__default  modal__x modal__call">
    <div class="modal__inner">
        <div class="modal__title">Оставьте заявку</div>
        <form action="javascript:void(0)" class="modal__form js_modal__form">

            <div class="form_field">
                <input type="text" name="name" required placeholder="Ваше имя">
            </div>

            <div class="form_field">
                <input type="tel" name="phone" required placeholder="Ваш телефон">
            </div>

            <div class="form_field">
                <textarea name="comment" placeholder="Комментарий"></textarea>
            </div>

            <div class="form_field form__field--btn">
                <button class="btn__default btn__orange btn__modal__call">Отправить</button>
            </div>

            <div class="form_field form__field--agree">
                Нажимая на кнопку, вы соглашаетесь на <a href="">обработку персональных данных</a>
            </div>
        </form>
    </div>
</div>



<!-- окно Спасибо Покупка в один клик -->

<!-- <div id="js_modal__thanks" class="modal__default modal__thanks">
    <div class="modal__inner">

        <svg class='modal__thanks__icon' width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="50" cy="50" r="48" stroke="#1356A2" stroke-width="4"/>
            <path d="M25 47.8182L42.3759 66L75 34" stroke="#1356A2" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>

        <div class="modal__title">Спасибо за ваш заказ!</div>
        <div class="modal__text">Мы свяжемся с вами в ближайшее время для уточнения деталей,<br> а также сообщим точную стоимость заказа.</div>

        <button class="btn__default btn__style__2 btn__form_close js__btn_close__modal">Хорошо, жду</button>

    </div>
</div> -->