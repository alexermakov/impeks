<?php include 'preloader.php'; ?>
<header class="js_header header_main <?php if ($homePage==true){ echo 'header_main--home';}?>">
    <div class="container js_animate_header <?php if ($homePage==true){ echo 'animate__header';}?>">

        <div class="header__search">
            <button class="js_header__search__btn header__search__btn">
                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M26.6658 26.6668L21.2658 21.2668M21.2658 21.2668C22.1326 20.4001 22.8201 19.3711 23.2892 18.2387C23.7582 17.1063 23.9996 15.8926 23.9996 14.6668C23.9996 13.4411 23.7582 12.2274 23.2892 11.0949C22.8201 9.96249 22.1326 8.93354 21.2658 8.06682C20.3991 7.20009 19.3702 6.51257 18.2377 6.0435C17.1053 5.57443 15.8916 5.33301 14.6658 5.33301C13.4401 5.33301 12.2264 5.57443 11.0939 6.0435C9.96152 6.51257 8.93257 7.20009 8.06584 8.06682C6.31541 9.81725 5.33203 12.1913 5.33203 14.6668C5.33203 17.1423 6.31541 19.5164 8.06584 21.2668C9.81627 23.0172 12.1904 24.0006 14.6658 24.0006C17.1413 24.0006 19.5154 23.0172 21.2658 21.2668Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                <span class="header__search__btn__text">Поиск</span>
            </button>
        </div>

        <a href="" class="header__logo">
            <img src="images/logo.svg" alt="main logo">
        </a>

        <div class="header__info">
            <div class="header__info__phone">
                <a href="tel:+73812386163">+7 (3812) 38-61-63</a>
            </div>

            <button class="header_btn_menu btn_menu js_btn_menu" aria-label="Open menu">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>



    </div>
</header>

<!-- end header -->
