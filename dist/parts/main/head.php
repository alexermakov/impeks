<?php $version = '0.0.0.38';?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">


<meta name="msapplication-TileColor" content="#224fc2">
<meta name="theme-color" content="#ffffff">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Roboto:ital,wght@0,400;0,500;1,300;1,400&display=swap" rel="stylesheet">


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css">



<link rel="stylesheet" href="css/vendor/select2.min.css">
<link rel="stylesheet" href="css/vendor/slick.css">
<link rel="stylesheet" href="css/vendor/fancybox.css">
<link rel="stylesheet" href="css/app.min.css?<?= $version;?>">