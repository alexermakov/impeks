<?php
$info = [
    [
        'title' => 'ТОЧКА-С',
        'text' => '<span>Вибрационное средства обнаружения для защиты периметра</span>',
        'img' => 'images/__content/pages/home/products/1.jpg'],
    [
        'title' => 'Второй проект',
        'text' => '<span>Lorem ipsum dolor sit amet consectetur adipisicing elit. </span>
                   <span>Veniam dolores expedita possimus dignissimos voluptas unde voluptatem explicabo, </span>
                   <span>Officia quaerat aut beatae sed? Adipisci, dolorem!</span>',
        'img' => 'images/__content/pages/home/products/2.jpg'
    ],
    [
        'title' => 'Проект K',
        'text' => '<span>Lorem ipsum dolor sit amet.</span>',
        'img' => 'images/__content/pages/home/products/3.jpg'
    ],
    [
        'title' => 'ТОЧКА-B',
        'text' => '<span>Вибрационное средства</span><span>обнаружения для защиты 21</span>',
        'img' => 'images/__content/pages/home/products/1.jpg'
    ],
    ['title' => 'ALICE',  'text' => '<span>Lorem ipsum dolor sit amet consectetur adipisicing elit. </span>
    <span>Veniam dolores expedita possimus dignissimos voluptas unde voluptatem explicabo, </span>
    <span>quisquam accusamus in perferendis omnis quos, ipsam quod autem tempora necessitatibus, ipsum quibusdam dolorum vero </span>
    <span>Officia quaerat aut beatae sed? Adipisci, dolorem!</span>',     'img' => 'images/__content/pages/home/products/2.jpg'
    ]
];
?>


<div class="home_products">
    <div class="container">
        <div class="title_x title_x--home_products js_animate__move_top animate__move_top">Наши продукты</div>
        <div class="home_products__main">

            <div class="home_products__slider__info js_home_products__slider__info">
                <div class="swiper-wrapper">
                    <?php foreach ($info as $key => $item) : ?>
                        <div class="home_products__slider__info__slide swiper-slide">
                            <div class="home_products__slide__info js_animate__move_top animate__move_top" data-delay='300'>
                                <div class="home_products__slide__title js_hp__slide__title"><?= $item['title']; ?></div>
                                <div class="home_products__slide__text"><?= $item['text']; ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php foreach ($info as $key => $item) : ?>
                        <div class="home_products__slider__info__slide swiper-slide">
                            <div class="home_products__slide__info js_animate__move_top animate__move_top" data-delay='300'>
                                <div class="home_products__slide__title js_hp__slide__title"><?= $item['title']; ?></div>
                                <div class="home_products__slide__text"><?= $item['text']; ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="home_products__slider__image js_home_products__slider__image">
                <div class="swiper-wrapper">
                    <?php foreach ($info as $key => $item) : ?>
                        <div class="swiper-slide hp__slide__image__block">
                            <div class="hp__slide__image__content">
                                <div class="hp__slide__image js_animate__move_top animate__move_top">
                                    <div class="hp__slide__image__inner">
                                        <img src="<?= $item['img']; ?>?1" alt="<?= $item['title']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php foreach ($info as $key => $item) : ?>
                        <div class="swiper-slide hp__slide__image__block">
                            <div class="hp__slide__image__content">
                                <div class="hp__slide__image js_animate__move_top animate__move_top">
                                    <div class="hp__slide__image__inner">
                                        <img src="<?= $item['img']; ?>?1" alt="<?= $item['title']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>