<div class="home_about__wrap">
    <div class="home_about">
        <div class="container">
            <div class="home_about__image__decore js_animate__move_top animate__move_top">
                <img src="images/pages/home/lens.png" alt="Немного о нас">
            </div>
            <div class="title_x js_animate__move_top animate__move_top" data-delay='0'>Немного о нас</div>
            <div class="home_about__info">
                <div class="home_about__info__item home_about__info__item--1 js_animate__move_top animate__move_top" data-delay='0'>
                    <div class="home_about__info__item__info">
                        <p>Компания ООО «Импэкс-Групп» - это стабильная, высокотехнологичная, динамично развивающаяся компания, которая осуществляет комплексные поставки оборудования для построения систем безопасности.</p>
                    </div>
                    <a href="" class="btn_default btn_glow btn_home_about"><span>Сертификаты</span></a>
                </div>
                <div class="home_about__info__item home_about__info__item--2 js_animate__move_top animate__move_top" data-delay='0'>
                    <div class="home_about__info__item__glow">
                        <p>Мы являемся прямым дистрибьютором ведущих российских производителей, а также ряда эксклюзивных представителей передовых зарубежных брендов. Нашими поставщиками являются более 100 отечественных и зарубежных производителей оборудования систем безопасности.</p>
                    </div>
                </div>
                <div class="home_about__info__item home_about__info__item--3 js_animate__move_top animate__move_top" data-delay='0'>
                    <div class="home_about__info__item__glow">
                        <p>Мы осуществляем отгрузку товара с более чем 10 складов по России и предоставляем полный комплекс услуг по доставке приобретенного оборудования в страны Таможенного союза практически любым видом транспорта.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="home_about__trigger js_home_about__trigger js_animate__move_top animate__move_top" data-delay='0'>
        <div class="container">
            <div class="home_about__trigger__list">
                <div class="home_about__trigger__list__inner js_home_about__trigger__list home_about__trigger__list__inner--desktop">
                    <?php
                        $info =[
                            ['100','Успешных проектов'],
                            ['100','Брендов систем безопасности'],
                            ['50','Постоянных клиентов'],
                            ['25','Мировых производителей'],
                            ['10','Складов по России'],
                            ['50','Постоянных клиентов'],
                        ];
                    ?>

                    <?php foreach ($info as $key => $item):?>
                        <div class="home_about__trigger__item">
                            <div class="home_about__trigger__item__inner">
                                <div class="home_about__trigger__item__number"><?= $item[0];?></div>
                                <div class="home_about__trigger__item__text"><?= $item[1];?></div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>

                <div class="home_about__trigger__list__inner home_about__trigger__list__inner--mobile">
                    <div class="home_about__trigger__list__inner--mobile__inner">
                        <?php foreach ($info as $key => $item):?>
                            <div class="home_about__trigger__item">
                                <div class="home_about__trigger__item__inner">
                                    <div class="home_about__trigger__item__number"><?= $item[0];?></div>
                                    <div class="home_about__trigger__item__text"><?= $item[1];?></div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>