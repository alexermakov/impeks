function getBodyScrollTop() {
	return (
		self.pageYOffset ||
		(document.documentElement && document.documentElement.ScrollTop) ||
		(document.body && document.body.scrollTop)
	)
}
let flagHideHeader = false

document.addEventListener('DOMContentLoaded', (event) => {
	document.body.classList.add('loaded')
})



$(function () {

	Fancybox.defaults.Thumbs = false;
	Fancybox.defaults.Toolbar = false;

	let lastScrollTop = 0
	$(window).scroll(function () {
		const $header = $('.js_header')
		let st = $(this).scrollTop()

		let addClassHide = st > lastScrollTop ? true : false


		let outterTop = $(window).width()>550 ? 0 : 10;
		if (st < outterTop) {
			addClassHide = false
		}



		if (!flagHideHeader) {
			if (addClassHide) {
				$header.addClass('header_hide')
				$header.removeClass('header_show')
			} else {
				$header.removeClass('header_hide')
				$header.addClass('header_show')
			}
			if (st == 0) {
				$header.removeClass('header_hide')
				$header.removeClass('header_show')
			}
		}

		lastScrollTop = st
	})

	$('.js_select').each(function () {
		let placeholder = $(this).attr('placeholder')
		$(this).select2({
			minimumResultsForSearch: 1 / 0,
			placeholder: placeholder,
			// allowClear: true
		})
	})



	const swiperSertificates = new Swiper('.js_sertificate__slider', {
		// loop: true,
		speed: 500,
		slidesPerView: 1.5,
		longSwipesRatio: 0.45,
		sticky: true,
		grid: {
			rows: 1,
			fill: 'cols',
		},

		breakpoints: {
			// when window width is >= 551
			551: {
				slidesPerView: 3,
				grid: {
					rows: 2,
					fill: 'cols',
				},
			},
			// when window width is >= 751
			751: {
				slidesPerView: 4,
				grid: {
					rows: 2,
					fill: 'cols',
				},
			},
		},

		loopAddBlankSlides: false,
		longSwipesRatio: 0.5,
		pagination: {
			clickable: true,
			el: '.js_sertificate__slider__dots',
			type: 'bullets',
		},
	})

	function SlideTitleWrap() {
		let index = 0
		$('.js_hp__slide__title').each(function () {
			let text = $(this).text()
			let textHtml = '';
			[...text].forEach((letter) => {
				if (index == 0) {
					textHtml += `<span class='show'>${letter}</span>`
				} else {
					textHtml += `<span>${letter}</span>`
				}
			})
			$(this).html(textHtml)
			index++
		})
	}
	SlideTitleWrap()

	function shuffle(array) {
		let currentIndex = array.length,
			randomIndex

		// While there remain elements to shuffle.
		while (currentIndex > 0) {
			// Pick a remaining element.
			randomIndex = Math.floor(Math.random() * currentIndex)
			currentIndex--

			// And swap it with the current element.
			;[array[currentIndex], array[randomIndex]] = [
				array[randomIndex],
				array[currentIndex],
			]
		}

		return array
	}

	const swiperHPinfo = new Swiper('.js_home_products__slider__info', {
		loop: true,
		speed: 1250,

		slidesPerView: 'auto',
		longSwipesRatio: 0.25,
		effect: 'fade',
		fadeEffect: {
			crossFade: true,
		},
		on: {
			init: function () {
				let activeSLide = this.slides[0]
				$(activeSLide).addClass('show')
			},
		},
	})

	const swiperHPimages = new Swiper('.js_home_products__slider__image', {
		loop: true,
		slidesPerView: 'auto',
		speed: 1250,
		slideToClickedSlide :true,

		longSwipesRatio: 0.25,
		keyboard: {
			enabled: true,
			onlyInViewport: false,
		},

	})
	swiperHPimages.snapGrid = swiperHPimages.slidesGrid.slice(0);


	function getRandomInt(max) {
		return Math.floor(Math.random() * max)
	}

	swiperHPimages.on('slideChangeTransitionStart', function () {
		let activeSLide = swiperHPinfo.slides[swiperHPimages.realIndex]
		$(activeSLide).removeClass('show')
		setTimeout(() => {
			$('.js_hp__slide__title span').removeClass('show')
		}, 300)

		swiperHPinfo.slideTo(swiperHPimages.realIndex)
	})

	swiperHPinfo.on('slideChangeTransitionStart', function () {
		let activeSLide = swiperHPinfo.slides[swiperHPimages.realIndex]
		$(activeSLide).addClass('show')
		let $title = $(activeSLide).find('.js_hp__slide__title')
		let $titleElements = $title.find('span')
		let indexList = []
		let timeAnimate = 80
		for (let index = 0; index < $titleElements.length; index++) {
			indexList.push(index)
		}
		shuffle(indexList)



		setTimeout(() => {


			for (let index = 0; index < indexList.length; index++) {
				setTimeout(() => {
					timeAnimate = getRandomInt(40) + 10
					$titleElements.eq(indexList[index]).addClass('show')
				}, timeAnimate * index)
			}
		}, 500);
	})

	swiperHPinfo.on('slideChangeTransitionEnd', function () {
		let activeSLide = swiperHPinfo.slides[swiperHPimages.realIndex]

	})

	$('.js_header__search__btn').click(function (e) {
		e.preventDefault()
		let topY = getBodyScrollTop()

		if (!$('body').hasClass('body-lock')){
		flagHideHeader = true;
			setTimeout(() => {
				// $('body').data('topY', topY)
				// $('body').css('top', '-' + topY + 'px')
				$('body').addClass('body-lock')
			}, 350);
		}

		$('.js__modal--search').addClass('active')
	})

	$('.js_modal__btn_close').click(function (e) {
		e.preventDefault()
		if($('.js__modal.active').length==1){
			$('body').removeClass('body-lock')
			// $('body').removeAttr('style')
			// window.scrollTo(0, $('body').data('topY'))
		}
		$(this).closest('.js__modal').removeClass('active')
		setTimeout(() => {
			flagHideHeader = false
		}, 200)
	})

	$('.js_btn_menu').click(function (e) {
		e.preventDefault()
		flagHideHeader = true
		let topY = getBodyScrollTop()
		setTimeout(() => {
			// $('body').data('topY', topY)
			// $('body').css('top', '-' + topY + 'px')
			$('body').addClass('body-lock')
		}, 350);

		$('.js__modal--menu').addClass('active')
	})

	$('.js_form_search__field').keyup(function (e) {
		e.preventDefault()
		let $input = $(this)
		let searchValue = $input.val().trim()
		let searchAjaxList = $('.js_modal__search__result__list')
		let searchAjaxListNONE = $('.js_modal__search__result__list--none')


		console.log(searchValue)
		if (searchValue.length!=0) {
			if (searchValue.length<5){
				searchAjaxList.addClass('active')
				searchAjaxListNONE.removeClass('active')
			} else {
				searchAjaxListNONE.addClass('active')
				searchAjaxList.removeClass('active')
			}
		}else{
			searchAjaxList.removeClass('active')
			searchAjaxListNONE.removeClass('active')
		}
	})

	$('.js_btn__search--reset').click(function () {
		let searchAjaxList = $('.js_modal__search__result__list')
		let searchAjaxListNONE = $('.js_modal__search__result__list--none')
		searchAjaxList.removeClass('active')
		searchAjaxListNONE.removeClass('active')
	})

	$('.js__btn_close__modal').click(function (e) {
		Fancybox.close()
	})

	$('.js__btn_open__modal').click(function (e) {
		Fancybox.show([
			{
				src: '#js_modal__call',
			},
		])
	})

	$('.js_pproduct__image__block__add .pproduct__image__block__add__item').hover(function () {
		let url = $(this).find('img').attr('src')
		$('.js_pproduct__image__block__main .pproduct__image__block__main__more').attr('data-src',url)
		$('.js_pproduct__image__block__main img').attr('src',url)
		}, function () {}
	);
})
