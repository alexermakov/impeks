let glitch__animationALL = null;

$(function () {


        if ($('.js_home_about__trigger__list').length){
            gsap.to('.js_home_about__trigger__list', {
                xPercent: -100,
                ease: 'power1.out',
                // stagger: 0.75,
                scrollTrigger: {
                    trigger: '.js_home_about__trigger',
                    start: 'top 87.5%',
                    end: 'top 12.5%',
                    scrub: 0.25,
                },
            })
        }

    if ($('.js_animate__move_top').length){
        $(".js_animate__move_top").each(function () {
            delay = $(this).attr('data-delay') ? $(this).data('data-delay') : 0;
            let el = $(this)[0];
            gsap.to(el, {
                scrollTrigger: {
                    trigger: el,
                    start: 'top 95%',
                },
                opacity: 1,
                y: '0',
                ease: 'power1.out',
                stagger: {
                    amount: 0.3
                },
                duration: 0.75,
                delay: delay

            })
        })
    }



    function loadedPreloader() {
        const allImages = document.querySelectorAll('img');
        const allImagesCount = allImages.length;
        let countImageLoaded = 0;


        const load = url => new Promise(resolve => {
            const img = new Image()
            img.onload = () => resolve({
                url
            })
            img.src = url;
            countImageLoaded++;
        });


        allImages.forEach(image => {
            (async () => {
                const {
                    url
                } = await load(image.src);
            })();
            if (allImagesCount == countImageLoaded) {
                setTimeout(() => {removeLoader()}, 0);

            }
        });
        function removeLoader() {
            setTimeout(() => {
                let timePreloader = 2.5;
                const path = $('.js_loader_x__svg__path')[0]

                path.setAttribute('stroke-dasharray', path.getTotalLength())
                path.setAttribute('stroke-dashoffset', path.getTotalLength())

                gsap.to(path, {
                    ease: 'linear',
                    strokeDashoffset: path.getTotalLength() * 2,
                    duration: timePreloader,
                })
                setTimeout(() => {
                    $('.js_loader_x__svg__dot').fadeIn(400)
                }, (timePreloader-0.5)*1000);


                setTimeout(() => {
                    $('.js_loader_x').addClass('showed')
                    animateAfterPreloader(500);
                    $('body').removeClass('body-lock')
                }, timePreloader*1000+200 );


            }, 500);
        }

    }

    function animateAfterPreloader(timeStartScreenAnimation=650){
        setTimeout(() => {

            setTimeout(() => {
                $(".js_animate__home_image").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        opacity:1,
                        ease: 'power1.out',
                        stagger: {
                            amount: 0.3
                        },
                        duration: 1.25

                    })
                })
            }, 100);



            $(".js_animate_header").each(function () {
                el = $(this)
                elParent = $(this).parent()[0];

                gsap.to(el, {
                    scrollTrigger: {
                        trigger: elParent,
                        start: 'top 85%',
                    },
                    y: '0',
                    opacity:1,
                    ease: 'power1.in',
                    stagger: {
                        amount: 0.3
                    },
                    duration: 0.6

                })
            })

            animateHomeTitle()



        }, timeStartScreenAnimation);
    }


    function shuffle(array) {
		let currentIndex = array.length,
			randomIndex

		// While there remain elements to shuffle.
		while (currentIndex > 0) {
			// Pick a remaining element.
			randomIndex = Math.floor(Math.random() * currentIndex)
			currentIndex--

			// And swap it with the current element.
			;[array[currentIndex], array[randomIndex]] = [
				array[randomIndex],
				array[currentIndex],
			]
		}

		return array
	}
    function getRandomInt(max) {
		return Math.floor(Math.random() * max)
	}

    async function animateHomeTitle(){

        let $title = $('.js_glitch__main__letter')
        let $titleElements = $title.find('path')
        let indexList = []
        let timeAnimate = 100
        for (let index = 0; index < $titleElements.length; index++) {
            indexList.push(index)
        }
        shuffle(indexList)



        for (let index = 0; index < indexList.length; index++) {
            timeAnimate = getRandomInt(70)*12;
            setTimeout(() => {
                $titleElements.eq(indexList[index]).addClass('show')
            }, timeAnimate)
        }

        setTimeout(() => {
            $('.js_home_hero__title__line').addClass('show')
        }, 900);

        setTimeout(() => {
            $('.js_home_hero__title__small__text').addClass('show')
            $('.js_home_hero__title__line').addClass('show2')

            glitch__animationALL = setInterval(() => {
                animetaGlitchAll()
            }, 7000);

        }, 1300);




    }


	$('.js_home_hero__title div').each(function () {
        if (!$(this).hasClass('glitch_title_home')){
		    let text = $(this).text()
            let textHtml = '';
            [...text].forEach((letter) => {
                textHtml += `<span>${letter}</span>`
            })
            $(this).html(textHtml)
        }
    })

    function animetaGlitchAll(){
        let timeDuration = 70;
        let elements = $('.js_glitch__animation--1 >g,.js_glitch__animation--2 >g');
        let timeAllAnimation = elements.length * timeDuration;

        elements.each(function(index,el){
            $('.js_glitch__main').addClass('hide')

            setTimeout(()=>{
                $(this).addClass('show')
            },index * timeDuration)

            setTimeout(()=>{
                $(this).removeClass('show')
            },(index * timeDuration)+timeDuration)
        })

        setTimeout(function(){$('.js_glitch__main').removeClass('hide')},timeAllAnimation)
    }


    function animetaGlitch(el){
        let timeDuration = 70;
        let timeAllAnimation = el.find('>g').length * timeDuration;

        el.find('>g').each(function(index,el){
            $('.js_glitch__main').addClass('hide')

            setTimeout(()=>{
                $(this).addClass('show')
            },index * timeDuration)

            setTimeout(()=>{
                $(this).removeClass('show')
            },(index * timeDuration)+timeDuration)


        })
        setTimeout(function(){$('.js_glitch__main').removeClass('hide')},timeAllAnimation)
    }

    $('.js_home_hero__title').hover(function () {
            clearInterval(glitch__animationALL)
        }, function () {
            glitch__animationALL = setInterval(() => {animetaGlitchAll()}, 7000);
        }
    );

    let delayAnimetaGlitch_1 = false
    let delayAnimetaGlitch_2 = false
    $('.js_glitch__main--1').hover(function () {
        if (!delayAnimetaGlitch_1) animetaGlitch($('.js_glitch__animation--1'))
        delayAnimetaGlitch_1 = true;
        setTimeout(() => {delayAnimetaGlitch_1 = false}, 1000);
    }, function () {});

    $('.js_glitch__main--2').hover(function () {
        if (!delayAnimetaGlitch_2) animetaGlitch($('.js_glitch__animation--2'))
        delayAnimetaGlitch_2 = true;
        setTimeout(() => {delayAnimetaGlitch_2 = false}, 1000);
    }, function () {});



    if ($('body').hasClass('home')){
        loadedPreloader()
    }
    else{
        animateAfterPreloader(0);
    }

})