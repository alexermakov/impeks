<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
            li {
                margin-bottom: 3px;
            }

            a {
                display: inline-block;
                padding: 3.5px 10px;
            }

            .good {
                background: #01d95e;
                color: black;
                text-decoration: none;
                display: inline-block;
            }

        </style>
    </head>

    <body>
        <ol>
            <li><a class="" href="index.php">Home</a></li>
            <li><a class="" href="catalog.php">Catalog</a></li>
            <li><a class="" href="product.php">Product</a></li>
            <li><a class="" href="decisions.php">Decisions</a></li>
            <li><a class="" href="decision.php">Decision</a></li>
            <li><a class="" href="vad.php">Вэд</a></li>
            <li><a class="good" href="contact.php">Contact</a></li>
            <li><a class="good" href="news.php">News</a></li>
            <li><a class="good" href="single.php">New item</a></li>
            <li><a class="" href="brands.php">Brands</a></li>
            <li><a class="" href="projects.php">Projects</a></li>
            <li><a class="" href="docs.php">Docs</a></li>
            <li><a class="" href="products.php">Products</a></li>
            <li><a class="good" href="search.php">Search</a></li>
            <li><a class="good" href="404.php">404</a></li>
        </ol>
    </body>

</html>
