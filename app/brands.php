<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Brands</title>
        <meta name="description" content="text">
        <?php include 'parts/main/head.php'; ?>

    </head>
    <body>
        <div class="page__wrap">
            <?php include 'parts/main/header.php'; ?>

            <div class="page__container page__container--default page__container--brands">
                <div class="container">
                    <h1 class="title_y">Бренды</h1>

                    <div class="brands__row">
                        <div class="brands__row__name__list js_brands__row__name__sticky">
                            <ul>
                               <li class='active'>Системы видеонаблюдения</li>
                               <li>Системы оповещения</li>
                               <li>Системы контроля доступа</li>
                               <li>Системы охраны периметра</li>
                               <li>Системы пожаротушения и огнезащита</li>
                               <li>Сетевое оборудование</li>
                               <li>Источники питания</li>
                            </ul>
                        </div>

                        <div class="brands__row__image__list">
                            <div class="brands__row__image__list__section">
                                <?php for ($i=1; $i <= 9; $i++):?>
                                    <div class="brands__row__image__item">
                                        <div class="brands__row__image__item__inner">
                                            <img src="images/__content/pages/brands/1/<?= $i;?>.png" alt="logo item">
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                            <div class="brands__row__image__list__section">
                                <?php for ($i=1; $i <= 3; $i++):?>
                                    <div class="brands__row__image__item">
                                        <div class="brands__row__image__item__inner">
                                            <img src="images/__content/pages/brands/2/<?= $i;?>.png" alt="logo item">
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                            <div class="brands__row__image__list__section">
                                <?php for ($i=1; $i <= 16; $i++):?>
                                    <div class="brands__row__image__item">
                                        <div class="brands__row__image__item__inner">
                                            <img src="images/__content/pages/brands/3/<?= $i;?>.png" alt="logo item">
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                    </div>




                </div>
            </div>

            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>
</html>