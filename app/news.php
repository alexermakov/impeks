<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Contact</title>
        <meta name="description" content="text">
        <?php include 'parts/main/head.php'; ?>

    </head>
    <body>
        <div class="page__wrap">
            <?php include 'parts/main/header.php'; ?>

            <div class="page__container page__container--default page__container--news">
                <div class="container">
                    <h1 class="title_y">Новости</h1>

                    <?php
                        $info = [
                            [
                                'title'=>'Региональный конкурс «Экспортер года»',
                                'date'=>'13 ИЮЛЯ ‘21',
                                'image'=>'images/__content/pages/news/list/1.jpg',
                                'text'=>'<p>6 июля состоялась церемония награждения регионального конкурса «Экспортер года»* в Омской области по итогам 2020 года.</p><p>Компания ООО «Импэкс-Групп» заняла почетное III место в номинации «Прорыв года!».</p>',
                            ],
                            [
                                'title'=>'Решение для управления парковкой. Контроль телекамер «рыбий глаз» Wisenet TNF-9010',
                                'date'=>'24 МАЯ ‘21',
                                'image'=>'images/__content/pages/news/list/2.jpg',
                                'text'=>'<p>Применение искусственного интеллекта в видеонаблюдении позволяет поднять на новый уровень контроль парковок. С помощью одной и той же панорамной камеры Wisenet TNF-9010, продукта компании Hanwha Techwin, выполняется охранный мониторинг и отслеживаются свободные места в любой момент времени.</p>',
                            ],
                            [
                                'title'=>'Новые скоростные камеры линейки Wisenet X PTZ Plus выполняют автотрекинг с помощью искусственного интеллекта',
                                'date'=>'11 МАРТА ‘21',
                                'image'=>'images/__content/pages/news/list/3.jpg',
                                'text'=>'<p>Шесть новых скоростных поворотных IP-камер с функцией автотрекинга пополнили линейку Wisenet X PTZ PLUS от компании Hanwha Techwin. Каждая из них позволяет вести видеонаблюдение на обширной территории с чётким обнаружением и отслеживанием объектов.</p>',
                            ],
                            [
                                'title'=>'TNU-6320E и TNO-6320E — серия взрывобезопасного оборудования для применения в нефтегазовой и энергетической отрасли.',
                                'date'=>'10 ЯНВАРЯ ‘19',
                                'image'=>'images/__content/pages/news/list/4.jpg',
                                'text'=>'<p>Шесть новых скоростных поворотных IP-камер с функцией автотрекинга пополнили линейку Wisenet X PTZ PLUS от компании Hanwha Techwin. Каждая из них позволяет вести видеонаблюдение на обширной территории с чётким обнаружением и отслеживанием объектов.</p>',
                            ],
                            [
                                'title'=>'Региональный конкурс «Экспортер года»',
                                'date'=>'13 ИЮЛЯ ‘21',
                                'image'=>'images/__content/pages/news/list/1.jpg',
                                'text'=>'<p>6 июля состоялась церемония награждения регионального конкурса «Экспортер года»* в Омской области по итогам 2020 года.</p><p>Компания ООО «Импэкс-Групп» заняла почетное III место в номинации «Прорыв года!».</p>',
                            ],
                            [
                                'title'=>' Решение для управления парковкой. Контроль безопасности и наличия свободных мест с помощью телекамер «рыбий глаз» Wisenet TNF-9010',
                                'date'=>'24 МАЯ ‘21',
                                'image'=>'images/__content/pages/news/list/2.jpg',
                                'text'=>'<p>Применение искусственного интеллекта в видеонаблюдении позволяет поднять на новый уровень контроль парковок. С помощью одной и той же панорамной камеры Wisenet TNF-9010, продукта компании Hanwha Techwin, выполняется охранный мониторинг и отслеживаются свободные места в любой момент времени.</p>',
                            ],
                        ];
                    ?>
                    <div class="news_page__list">
                        <?php foreach ($info as $key => $item):?>
                            <div class="news_page__item">
                                <a href='' class="news_page__item__image">
                                    <div class="news_page__item__image__inner">
                                        <img src="<?= $item['image'];?>" alt="<?= $item['title'];?>">
                                    </div>
                                </a>
                                <div class="news_page__item__info">
                                    <div class="news_page__item__date"><?= $item['date'];?></div>
                                    <div class="news_page__item__title">
                                        <a href=""><?= $item['title'];?></a>
                                    </div>
                                    <div class="news_page__item__text">
                                        <?= $item['text'];?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>

                </div>

                <?php $pagintaion_class = 'pagination--news';?>
                <?php include 'parts/components/pagination.php'; ?>
            </div>

            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>
</html>