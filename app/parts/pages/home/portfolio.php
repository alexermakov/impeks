<?php
    $info = [
        ['Система охранного освещения'],
        ['Системы охраны периметра'],
        ['Системы видеонаблюдения'],
        ['Системы оповещения'],
        ['Системы контроля доступа, включая аудио и видеодомофоны'],
        ['Системы охранно-пожарной сигнализации'],
        ['Программное обеспечение для систем безопасности'],
        ['Монтажные материалы и СКС']
    ];
?>

<div class="home_portfolio">
    <div class="container">
        <div class="title_x title_x--home_portfolio js_animate__move_top animate__move_top">Портфель нашей продукции</div>
        <div class="text_x text_x--home_portfolio js_animate__move_top animate__move_top">
            <p>Портфель продукции ООО «Импэкс-Групп» охватывает полный спектр оборудования технических средств безопасности и включает в себя более 100 брендов</p>
        </div>

        <div class="home_portfolio__list">

            <?php foreach ($info as $key => $item) : ?>
                <?php $title = $item[0];?>
                <div class="home_portfolio__item">
                    <div class="container js_animate__move_top animate__move_top">
                        <div class="home_portfolio__item__inner">
                            <div class="home_portfolio__item__title">
                                <a href=""><?= $title;?></a>
                            </div>
                            <div class="home_portfolio__item__image">
                                <img src="images/__content/pages/home/products_2/1.jpg" alt="<?= $title;?>" loading="lazy">
                            </div>
                            <div class="home_portfolio__item__dot"></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

    </div>
</div>