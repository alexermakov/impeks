
<div class="home_sertificate">
    <div class="container">
        <div class="title_x title_x--home_sertificate js_animate__move_top animate__move_top">Благодарственные письма</div>

        <div class="home_sertificate__main">

            <div class="home_sertificate__main__slider js_sertificate__slider">

                <div class="swiper-wrapper">
                    <?php for ($i=0; $i < 15; $i++):?>
                        <div class="sertificate__slide__item swiper-slide">
                            <div class="sertificate__slide__item__content js_animate__move_top animate__move_top" data-fancybox='#sertificates' data-src='images/__content/pages/home/docs/1.jpg'>
                                <div class="sertificate__slide__item__content__inner">
                                    <img src="images/__content/pages/home/docs/1.jpg"  alt='sertificate - <?= $i;?>'>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

            </div>
            <div class="home_sertificate__main__bottom js_animate__move_top animate__move_top">
                <a href="" class="btn_default btn_glow btn_home_sertificate__main"><span>Референс лист</span></a>
                <div class="home_sertificate__main__dots">
                    <div class="dots_x js_sertificate__slider__dots"></div>
                </div>
            </div>

        </div>


    </div>
</div>