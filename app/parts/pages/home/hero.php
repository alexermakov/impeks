<div class="home_hero">
    <div class="home_hero__bg animate_opacity js_animate__home_image">
        <img src="images/pages/home/bg/0.png" alt="bg hero blur">
    </div>
    <div class="container">
        <div class="home_hero__content">

            <div class="home_hero__image animate_opacity js_animate__home_image home_hero__image--mobile">
                <img src="images/pages/home/lock--mobile.png" alt="Hero image">
            </div>

            <div class="home_hero__info">
                <div class="home_hero__title__wrap">
                    <div class="home_hero__title js_home_hero__title">
                        <?php include 'hero__glitch.php'; ?>
                    </div>
                </div>
            </div>
            <div class="home_hero__image animate_opacity js_animate__home_image home_hero__image--desktop">
                <img src="images/pages/home/lock.png" alt="Hero image">
            </div>
        </div>
    </div>
</div>