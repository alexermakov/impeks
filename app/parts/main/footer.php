<!-- start footer -->

<footer class="main_footer">
    <div class="container">

        <div class="footer_top">
            <div class="footer_row">
                <div class="footer_col footer_col--1 js_animate__move_top animate__move_top">
                    <a href="" class="footer_logo">
                        <img src="images/logo--white.svg" alt='logo white'>
                    </a>
                    <div class="footer_contact">
                        <div class="footer_contact__phone">
                            <a href="tel:+73812386163">+7 (3812) 38-61-63</a>
                        </div>
                        <div class="footer_contact__mail">
                            <a href="mailto:info@impeks-group.ru">info@impeks-group.ru</a>
                        </div>
                        <div class="footer_contact__adress">
                            <p>Россия, г. Омск, ул. К. Маркса 41/1, офис 451</p>
                        </div>
                        <a href="https://wa.me/79037666206" class="footer_contact__wa__link btn_default">
                            <span>Написать нам в WhatsApp</span>
                        </a>
                    </div>
                </div>
                <div class="footer_col footer_col--2 js_animate__move_top animate__move_top">
                    <div class="footer_menu__block js_footer_menu__block">
                        <div class="footer_menu__block__title color--blue">Каталог</div>
                        <div class="footer_menu__block__list">
                            <ul>
                                <li><a href="">Видеонаблюдение</a></li>
                                <li><a href="">Видеорегистраторы</a></li>
                                <li><a href="">СКУД</a></li>
                                <li><a href="">Охранное освещение</a></li>
                                <li><a href="">Охрана периметра</a></li>
                                <li><a href="">ПО</a></li>
                                <li><a href="">Сетевое обуродование</a></li>
                                <li><a href="">IP-телефония</a></li>
                                <li><a href="">Кабель</a></li>
                                <li><a href="">Блоки питания</a></li>
                                <li><a href="">Шлагбаумы</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer_col footer_col--3 js_animate__move_top animate__move_top">
                    <div class="footer_menu__block js_footer_menu__block">
                        <div class="footer_menu__block__title color--blue">
                            <a href="">Решения</a>
                        </div>
                        <div class="footer_menu__block__list">
                            <ul>
                                <li><a href="">Банки</a></li>
                                <li><a href="">Нефтегазовый сектор</a></li>
                                <li><a href="">Образовательные объекты</a></li>
                                <li><a href="">Объект культуры</a></li>
                                <li><a href="">Объекты торговли</a></li>
                                <li><a href="">Объекты транспорта</a></li>
                                <li><a href="">Офисные центры</a></li>
                                <li><a href="">Промышленые объекты</a></li>
                                <li><a href="">Спортивные объекты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer_col footer_col--4 js_animate__move_top animate__move_top">
                    <div class="footer_menu__block js_footer_menu__block">
                        <div class="footer_menu__block__title color--blue">
                            <a href="">Аутсорсинг ВЭД</a>
                        </div>
                        <div class="footer_menu__block__list footer_menu__block__list--color_blue">
                            <ul>
                                <li><a href="">Проекты</a></li>
                                <li><a href="">Новости</a></li>
                                <li><a href="">Бренды</a></li>
                                <li><a href="">Документы</a></li>
                                <li><a href="">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer_col footer_col--5 js_animate__move_top animate__move_top">
                    <div class="footer_menu__block js_footer_menu__block">
                        <div class="footer_menu__block__title color--blue">
                            <a href="">Продукты</a>
                        </div>
                        <div class="footer_menu__block__list">
                            <ul>
                                <li><a href="">ВСО “Точка-С”</a></li>
                                <li><a href="">АСО “КОНТУР-С”</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer_bottom js_animate__move_top animate__move_top">
            <div class="footer_copyright"> 2010-2017 © Все права защищены.</div>
            <div class="footer_politic">
                <a href=""> Пользовательское соглашение</a>
            </div>
        </div>

    </div>
</footer>


<?php include 'modal.php'; ?>


<script src="js/vendor/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
<script src="js/vendor/fancybox.umd.js"></script>
<script src="js/vendor/gsap.min.js"></script>
<!-- <script src="js/vendor/jquery.inputmask.bundle"></script> -->
<script src="js/vendor/ScrollTrigger.min.js"></script>
<script src="js/vendor/select2.min.js"></script>
<!-- <script src="js/vendor/slick.min.js"></script> -->
<script src="js/app.min.js?<?= $version;?>"></script>
<script src="js/animate.js?<?= $version;?>"></script>