<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Contact</title>
        <meta name="description" content="text">
        <?php include 'parts/main/head.php'; ?>

    </head>
    <body>
        <div class="page__wrap">
            <?php include 'parts/main/header.php'; ?>

            <div class="page__container page__container--default page__container--search">
                <div class="container">
                    <h1 class="title_y">Результаты поиска</h1>
                    <div class="page_search__list">

                        <div class="page_search__block">
                            <div class="page_search__block__title">Продукты</div>
                            <div class="page_search__block__section">
                                <ul>
                                    <li><a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a></li>
                                    <li><a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a></li>
                                    <li><a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="page_search__block">
                            <div class="page_search__block__title">Каталог</div>
                            <div class="page_search__block__section">
                                <ul>
                                    <li>
                                        <a href="">
                                            <img src="images/__content/any/search_result.jpg" alt="Вибрационное средство обнаружения «Точка-С»">
                                            <span>Вибрационное средство обнаружения «Точка-С»</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="images/__content/any/search_result.jpg" alt="Вибрационное средство обнаружения «Точка-С»">
                                            <span>Вибрационное средство обнаружения «Точка-С»</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="page_search__block">
                            <div class="page_search__block__title">Статьи</div>
                            <div class="page_search__block__section">
                                <ul>
                                    <li><a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a></li>
                                    <li><a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a></li>
                                    <li><a href=""><span>Вибрационное средство обнаружения «Точка-С»</span></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <?php $pagintaion_class = 'pagination--search__list';?>
                    <?php include 'parts/components/pagination.php'; ?>
                </div>

                <?php $companies_class = 'companies_block--search';?>
                <?php include 'parts/pages/home/partners.php'; ?>
            </div>



            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>
</html>