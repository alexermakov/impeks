<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Contact</title>
        <meta name="description" content="text">
        <?php include 'parts/main/head.php'; ?>

    </head>
    <body>
        <div class="page__wrap">
            <?php include 'parts/main/header.php'; ?>

            <div class="page__container page__container--default page__container--contact">
                <div class="container">
                    <h1 class="title_y">Контакты</h1>

                    <div class="contact__block_1">
                        <div class="contact__block_1__row">
                            <div class="contact__block_1__col contact__block_1__col--1">
                                <div class="contact__block_1__item contact__block_1__item--big">
                                    <div class="contact__block_1__item__title--big contact__block_1__item__title">Наш адрес</div>
                                    <div class="contact__block_1__text">
                                        <div class="contact__block_1__adress">г. Омск, ул. К. Маркса, 41/1, офис 451</div>
                                    </div>
                                </div>
                                <div class="contact__block_1__item contact__block_1__item--big">
                                    <div class="contact__block_1__item__title--big contact__block_1__item__title">Телефон офиса</div>
                                    <div class="contact__block_1__text">
                                        <div class="contact__block_1__phone">
                                            <a href="tel:+73812386163">+7 (3812) 38-61-63</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contact__block_1__col contact__block_1__col--2">
                                <div class="contact__block_1__item">
                                    <div class="contact__block_1__item__title">Отдел продаж</div>
                                    <div class="contact__block_1__text">
                                        <div class="contact__block_1__text__links">
                                            <a href="tel:+73812386163">+7 (3812) 38-61-63</a>
                                            <a href="mailto:info@impeks-group.ru">info@impeks-group.ru</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact__block_1__item">
                                    <div class="contact__block_1__item__title">Бухгалтерия</div>
                                    <div class="contact__block_1__text">
                                        <div class="contact__block_1__text__links">
                                            <a href="tel:+73812386163">+7 (3812) 38-61-63</a>
                                            <a href="mailto:fin@impeks-group.ru">fin@impeks-group.ru</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contact__block_1__col contact__block_1__col--3">
                                <div class="contact__block_1__item">
                                    <div class="contact__block_1__item__title">Техническая поддержка</div>
                                    <div class="contact__block_1__text">
                                        <div class="contact__block_1__text__links">
                                            <a href="tel:+79659700472">+7 (965) 970-04-72</a>
                                            <a href="mailto:support@impeks-group.ru">support@impeks-group.ru</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact__block_1__item contact__block_1__item--ghost_btn">
                                    <a href="https://wa.me/79037666206" class="btn_default btn_glow btn_contact_x"><span>Написать нам в WhatsApp</span></a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="contact__block_2">
                        <h2 class="contact__block_2__title">Реквизиты</h2>
                        <div class="contact__block_2__list">

                            <div class="contact__block_2__section">
                                <div class="contact__block_2__section__title">Полное наименование </div>
                                <div class="contact__block_2__section__content">
                                    <p>Общество с ограниченной ответственностью«Импэкс-Групп»</p>
                                </div>
                            </div>

                            <div class="contact__block_2__section">
                                <div class="contact__block_2__section__title">Юридический адрес</div>
                                <div class="contact__block_2__section__content">
                                    <p>644042, Российская Федерация, г. Омск, пр. Карла Маркса, дом 41, Литер Е, офис 451</p>
                                </div>
                            </div>

                            <div class="contact__block_2__section">
                                <div class="contact__block_2__section__title">Фактический (почтовый) адрес</div>
                                <div class="contact__block_2__section__content">
                                    <p>644042, Российская Федерация, г. Омск, пр. Карла Маркса, дом 41, Литер Е, офис 451</p>
                                    <p>
                                        ИНН 5503150330 <br>
                                        КПП 550401001<br>
                                        ОГРН 1155543041610</p>
                                    <p>
                                        Счет № 40702810500830000948 <br>
                                        Банк: ПАО СКБ Приморья "Примсоцбанк"<br>
                                        БИК 040507803<br>
                                        к/с 30101810200000000803<br>
                                        SWIFT: PRMTRU8V
                                    </p>
                                </div>
                            </div>

                            <div class="contact__block_2__section">
                                <div class="contact__block_2__section__title">Телефон/факс</div>
                                <div class="contact__block_2__section__content">
                                    <p><a href="tel:+73812386163">+7 (3812) 38-61-63</a></p>
                                </div>
                            </div>

                            <div class="contact__block_2__section">
                                <div class="contact__block_2__section__title">E-mail</div>
                                <div class="contact__block_2__section__content">
                                    <p><a href="mailto:info@impeks-group.ru">info@impeks-group.ru</a></p>
                                </div>
                            </div>

                            <div class="contact__block_2__section">
                                <div class="contact__block_2__section__title">Директор</div>
                                <div class="contact__block_2__section__content">
                                    <p>Мавлютов Тимур Имилевич, действующий наосновании Устава</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="contact__block_doc">
                        <div class="contact__block__doc__list">
                            <a href='' download class="contact__block__doc__item">
                                <div class="contact__block__doc__item__icon">
                                    <svg width="40" height="41" viewBox="0 0 40 41" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <mask id="mask0_66_6903" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="0" y="0" width="40" height="41">
                                        <path d="M40 0.5H0V40.5H40V0.5Z" fill="white"/>
                                        </mask>
                                        <g mask="url(#mask0_66_6903)">
                                        <path d="M10 0.5C8.625 0.5 7.5 1.625 7.5 3V38C7.5 39.375 8.625 40.5 10 40.5H35C36.375 40.5 37.5 39.375 37.5 38V10.5L27.5 0.5H10Z" fill="#E2E5E7"/>
                                        <path d="M30 10.5H37.5L27.5 0.5V8C27.5 9.375 28.625 10.5 30 10.5Z" fill="#B0B7BD"/>
                                        <path d="M37.5 18L30 10.5H37.5V18Z" fill="#CAD1D8"/>
                                        <path d="M32.5 33C32.5 33.6875 31.9375 34.25 31.25 34.25H3.75C3.0625 34.25 2.5 33.6875 2.5 33V20.5C2.5 19.8125 3.0625 19.25 3.75 19.25H31.25C31.9375 19.25 32.5 19.8125 32.5 20.5V33Z" fill="#F15642"/>
                                        <path d="M7.94922 24.1822C7.94922 23.8522 8.20922 23.4922 8.62797 23.4922H10.9367C12.2367 23.4922 13.4067 24.3622 13.4067 26.0297C13.4067 27.6097 12.2367 28.4897 10.9367 28.4897H9.26797V29.8097C9.26797 30.2497 8.98797 30.4984 8.62797 30.4984C8.29797 30.4984 7.94922 30.2497 7.94922 29.8097V24.1822ZM9.26797 24.7509V27.2409H10.9367C11.6067 27.2409 12.1367 26.6497 12.1367 26.0297C12.1367 25.3309 11.6067 24.7509 10.9367 24.7509H9.26797Z" fill="white"/>
                                        <path d="M15.3638 30.4983C15.0338 30.4983 14.6738 30.3183 14.6738 29.8795V24.202C14.6738 23.8433 15.0338 23.582 15.3638 23.582H17.6526C22.2201 23.582 22.1201 30.4983 17.7426 30.4983H15.3638ZM15.9938 24.802V29.2795H17.6526C20.3513 29.2795 20.4713 24.802 17.6526 24.802H15.9938Z" fill="white"/>
                                        <path d="M23.7411 24.8833V26.472H26.2898C26.6498 26.472 27.0098 26.832 27.0098 27.1808C27.0098 27.5108 26.6498 27.7808 26.2898 27.7808H23.7411V29.8795C23.7411 30.2295 23.4923 30.4983 23.1423 30.4983C22.7023 30.4983 22.4336 30.2295 22.4336 29.8795V24.202C22.4336 23.8433 22.7036 23.582 23.1423 23.582H26.6511C27.0911 23.582 27.3511 23.8433 27.3511 24.202C27.3511 24.522 27.0911 24.882 26.6511 24.882H23.7411V24.8833Z" fill="white"/>
                                        <path d="M31.25 34.25H7.5V35.5H31.25C31.9375 35.5 32.5 34.9375 32.5 34.25V33C32.5 33.6875 31.9375 34.25 31.25 34.25Z" fill="#CAD1D8"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="contact__block__doc__item__info">
                                    <div class="contact__block__doc__item__info__title">Скачать реквизиты</div>
                                    <div class="contact__block__doc__item__info__text">Скачать, PDF 262кб</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>
</html>